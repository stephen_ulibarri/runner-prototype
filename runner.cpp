#include "raylib.h"

struct Runner
{
    int width{};
    int height{};
    int x{};
    int y{};
    float velocity{};
    float jumpVelocity{-70'000.f};
    bool isInAir{};

    // Animation properties
    float runningTime{};
    const float updateTime{1.f/12.f};
    int frameCount{};
    float animX{};
    int spriteSheetFrames{6};
};

struct Hazard
{
    int width{};
    int height{};
    int x{};
    int y{};

    // Animation properties
    float runningTime{};
    const float updateTime{1.f/12.f};
    int frameCount{};
    float animX{};
    int spriteSheetFrames{8};
};

struct FinishLine
{
    int width{};
    int height{};
    int x{};
    int y{};
};

bool IsRunnerOnGround(Runner run, int winHeight);
void ApplyGravity(Runner& run, int winHeight, float gravity);
void UpdateAnimFrame(Runner& run);
void UpdateAnimFrame(Hazard& haz);
bool CollisionDetection(Runner& run, Hazard nebulae[], int numHazards);
bool CollisionDetection(Runner& run, FinishLine& line, int padding);
bool CollisionDetection(Runner& run, Hazard& hazard, int padding);

int main()
{
    // Window Dimensions
    const int windowWidth{512};
    const int windowHeight{450};

    // Physics properties
    const float gravityAcceleration{2'500};

    // Initialize the window
    InitWindow(windowWidth, windowHeight, "Runner Game");

    // Load background textures
    Texture2D background = LoadTexture("textures/far-buildings.png");
    Texture2D midground = LoadTexture("textures/back-buildings.png");
    Texture2D foreground = LoadTexture("textures/foreground.png");

    // X coordinate for each background texture
    float backX{};
    float midX{};
    float foreX{};

    // Create a Runner data structure
    Runner runner;
    runner.x = windowWidth/3;
    runner.y = windowHeight/2;

    // Load the scarfy texture
    Texture2D scarfy = LoadTexture("textures/scarfy.png");
    const float scarfyWidth{(float)scarfy.width/6};
    const float scarfyHeight{(float)scarfy.height};

    // Set runner's dimensions based on texture
    runner.width = scarfyWidth;
    runner.height = scarfyHeight;

    // Create hazard structs
    Hazard hazard0;
    Hazard hazard1;
    Hazard hazard2;
    Hazard hazard3;
    Hazard hazard4;
    Hazard hazard5;
    // Create an array of hazards
    Hazard hazards[6] = {hazard0, hazard1, hazard2, hazard3, hazard4, hazard5};

    // Load Hazard texture
    Texture2D nebula = LoadTexture("textures/12_nebula_spritesheet.png");
    const float nebWidth{(float)nebula.width/8};
    const float nebHeight{(float)nebula.height/8};


    for (int i = 0; i < 6; i++)
    {
        hazards[i].y = windowHeight - nebHeight;
        hazards[i].width = nebWidth;
        hazards[i].height = nebHeight;
    }
    // Set their x locations
    hazards[0].x = windowWidth;
    hazards[1].x = hazards[0].x + 1000;
    hazards[2].x = hazards[1].x + 1350;
    hazards[3].x = hazards[2].x + 485;
    hazards[4].x = hazards[3].x + 550;
    hazards[5].x = hazards[4].x + 1450;

    // Create the finish line
    FinishLine finishLine;
    finishLine.x = hazards[5].x + 500;
    finishLine.y = 0;
    finishLine.width = 200;
    finishLine.height = windowHeight;
    
    SetTargetFPS(60);
    while (!WindowShouldClose())
    {
        // Update background X coordinates (texure is scaled 2x)
        backX -= 0.1f;
        if (backX <= -background.width*2) backX = 0;
        midX -= 2.5f;
        if (midX <= -midground.width*2) midX = 0;
        foreX -= 4.0f;
        if (foreX <= -foreground.width*2) foreX = 0;

        

        BeginDrawing();
        ClearBackground(WHITE);

        Vector2 background1Pos{backX, 20.f};
        DrawTextureEx(background, background1Pos, 0.0f, 2.0f, WHITE); // scaled 2x
        Vector2 background2Pos{background.width*2 + backX, 20.f};
        DrawTextureEx(background, background2Pos, 0.0f, 2.0f, WHITE); // scaled 2x

        // Draw the midground twice
        Vector2 midground1Pos{midX, 20.f};
        DrawTextureEx(midground, midground1Pos, 0.0f, 2.0f, WHITE); // scaled 2x
        Vector2 midground2Pos{midground.width*2 + midX, 20.f};
        DrawTextureEx(midground, midground2Pos, 0.0f, 2.0f, WHITE); // scaled 2x

        // Draw the foreground twice
        Vector2 foreground1Pos{foreX, 70.f};
        DrawTextureEx(foreground, foreground1Pos, 0.0f, 2.0f, WHITE); // scaled 2x
        Vector2 foreground2Pos{foreground.width*2 + foreX, 70.f};
        DrawTextureEx(foreground, foreground2Pos, 0.0f, 2.0f, WHITE); // scaled 2x

        // Perform collision detection with enemies
        if (CollisionDetection(runner, hazards, 6))
        {
            DrawText("Game Over!", windowWidth/4, windowHeight/2, 60, RED);
        }
        else if (CollisionDetection(runner, finishLine, 0))
        {
            DrawText("You Win!", windowWidth/4, windowHeight/2, 60, RED);
        }
        else
        {
            // Update runner's Animation Frame
            UpdateAnimFrame(runner);
            // Update physics and get runner's position
            ApplyGravity(runner, windowHeight, gravityAcceleration);


            // Update finish line
            finishLine.x -= 6;


            // Draw the sprite
            Rectangle scarfyRec{runner.frameCount*scarfyWidth, 0.f, scarfyWidth, scarfyHeight};
            Vector2 scarfyPos{(float)runner.x, (float)runner.y};
            DrawTextureRec(scarfy, scarfyRec, scarfyPos, WHITE);

            // Update and Draw the hazards
            Rectangle hazardRecs[6]{};
            for (int i = 0; i < 6; i++)
            {
                UpdateAnimFrame(hazards[i]);
                hazardRecs[i] = {hazards[0].frameCount*nebWidth, 0.f, nebWidth, nebHeight};
                hazards[i].x -= 6;
                Vector2 Pos{(float)hazards[i].x, (float)hazards[i].y};
                DrawTextureRec(nebula, hazardRecs[i], Pos, WHITE);
            }
        }
        EndDrawing();
    }

    UnloadTexture(scarfy);
    CloseWindow();
}

bool IsRunnerOnGround(Runner run, int winHeight)
{
    return run.y >= (winHeight - run.height);
}

void ApplyGravity(Runner& run, int winHeight, float gravity)
{
    float deltaTime{GetFrameTime()};
        if (IsRunnerOnGround(run, winHeight))
        {
            run.isInAir = false;
            if (IsKeyPressed(KEY_SPACE))
            {
                // Launch runner up!
                run.velocity += (gravity + run.jumpVelocity) * deltaTime;
                run.y += run.velocity * deltaTime;
            }
            else
            {
                run.y = winHeight - run.height;
                run.velocity = 0;
            }
        }
        else
        {
            run.isInAir = true;
            run.velocity += gravity * deltaTime;
            run.y += run.velocity * deltaTime;
        }
}

void UpdateAnimFrame(Runner& run)
{
    if (run.isInAir)
    {
        run.frameCount = 3;
        run.runningTime = 0;
    }
    else
    {
        const float deltaTime{GetFrameTime()};
        run.runningTime += deltaTime;
        if (run.runningTime >= run.updateTime)
        {
            // Time to use next anim frame
            run.frameCount++;
            run.runningTime = 0;
            if (run.frameCount > 5) run.frameCount = 0;
        }
    }
}

void UpdateAnimFrame(Hazard& haz)
{
    const float deltaTime{GetFrameTime()};
    haz.runningTime += deltaTime;
    if (haz.runningTime >= haz.updateTime)
    {
        // Time to use next anim frame
        haz.frameCount++;
        haz.runningTime = 0;
        if (haz.frameCount > haz.spriteSheetFrames) haz.frameCount = 0;
    }
}

bool CollisionDetection(Runner& run, Hazard nebulae[], int numHazards)
{
    for (int i = 0; i < numHazards; i++)
    {
        bool collision = CollisionDetection(run, nebulae[i], 25);

        if (collision)
        {
            return true;
        }
    }
    return false;
}

bool CollisionDetection(Runner& run, FinishLine& line, int padding)
{
    int runLeftX = run.x;
    int runRightX = run.x + run.width;
    int runUpperY = run.y;
    int runLowerY = run.y + run.height;

    int lineLeftX = line.x + padding;
    int lineRightX = line.x + line.width - padding;
    int lineUpperY = line.y + padding;
    int lineLowerY = line.y + line.height - padding;

    bool collision =    runLowerY >= lineUpperY &&
                        runUpperY <= lineLowerY &&
                        runLeftX <= lineRightX &&
                        runRightX >= lineLeftX;

    if (collision)
    {
        return true;
    }
    return false;
}

bool CollisionDetection(Runner& run, Hazard& haz, int padding)
{
    int runLeftX = run.x;
    int runRightX = run.x + run.width;
    int runUpperY = run.y;
    int runLowerY = run.y + run.height;

    int hazLeftX = haz.x + padding;
    int hazRightX = haz.x + haz.width - padding;
    int hazUpperY = haz.y + padding;
    int hazLowerY = haz.y + haz.height - padding;

    bool collision =    runLowerY >= hazUpperY &&
                        runUpperY <= hazLowerY &&
                        runLeftX <= hazRightX &&
                        runRightX >= hazLeftX;

    if (collision)
    {
        return true;
    }
    return false;
}